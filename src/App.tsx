import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createDraft, fetchNotes, selectNote, editNote } from './store/actions';
import { allNotes, draftNote, selectedNote } from './store/selectors';
import { ListView, ListItem } from './components/notes-list';
import NoteDetail from './components/NoteDetail/NoteDetail';
import NoteEdit from './components/NoteEdit/NoteEdit';
import ActionToolbar from './components/ActionToolbar/ActionToolbar';
import styles from './App.module.scss';
import { INoteDraft } from './models';

const App: React.FC = () => {
  const dispatch        = useDispatch(),
        notes           = useSelector(allNotes),
        note            = useSelector(selectedNote),
        draft           = useSelector(draftNote),
        onListItemClick = (id: number) => dispatch(selectNote(id)),
        onAddBtnClick   = (draft: INoteDraft) => dispatch(createDraft(draft)),
        onEditBtnClick  = (id: number) => dispatch(editNote(id));

  useEffect(() => { dispatch(fetchNotes.request()); }, [dispatch]);

  return (
    <div className={styles.app}>
      <header>
        Notes App
      </header>

      <aside>
        <ActionToolbar onAddBtnClick={onAddBtnClick}
                       onEditBtnClick={onEditBtnClick}/>
        <ListView>{
          notes.map(note =>
            <ListItem key={note.id}
                      item={note}
                      onListItemClick={onListItemClick}
            />
          )
        }</ListView>
      </aside>

      <main>
        {!!note ? <NoteDetail note={note}/> : <span>Select a note</span>}
        {!!draft && <NoteEdit draft={draft}/>}
      </main>

      <footer>
        Footer
      </footer>
    </div>
  );
};

export default App;
