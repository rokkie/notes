import { combineReducers } from 'redux';
import * as notes from './notes';

export default combineReducers(Object.assign({}, notes));
