import { ActionType, createReducer } from 'typesafe-actions';
import * as actions from '../actions';
import { INote, INoteDraft } from '../../models';
import {
  NOTES_CREATE_DRAFT,
  NOTES_EDIT,
  NOTES_FETCH_SUCCESS, NOTES_SAVE_FAILURE,
  NOTES_SAVE_SUCCESS,
  NOTES_SELECT,
} from '../actions/types';

export interface INoteState {
  notes: Map<number, INote>;
  selected: number | null;
  draft?: INoteDraft;
}

const initalState = {
  notes   : new Map(),
  selected: null,
};

export const notes = createReducer<INoteState, ActionType<typeof actions>>(initalState, {
  [NOTES_FETCH_SUCCESS]: (state, action) => ({
    ...state,
    notes: new Map(action.payload.map(note => [note.id, note])),
  }),

  [NOTES_SAVE_SUCCESS]: (state, action) => ({
    ...state,
    notes: new Map(state.notes.set(action.payload.id, action.payload)),
  }),

  [NOTES_SAVE_FAILURE]: (state, action) => {
    console.error(action.payload);
    return state;
  },

  [NOTES_SELECT]: (state, action) => ({
    ...state,
    selected: action.payload,
  }),

  [NOTES_EDIT]: (state, action) => ({
    ...state,
    draft: state.notes.has(action.payload)
      ? Object.assign({}, state.notes.get(action.payload))
      : undefined,
  }),

  [NOTES_CREATE_DRAFT]: (state, action) => ({
    ...state,
    draft: action.payload,
  }),
});
