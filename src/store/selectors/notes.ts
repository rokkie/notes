import { INote } from '../../models';
import { INoteState } from '../reducers/notes';

type IState = {
  notes: INoteState;
}

export const allNotes = (state: IState): INote[] => Array.from(state.notes.notes.values());

export const selectedNote = (state: IState) => state.notes.selected && state.notes.notes.has(state.notes.selected) && state.notes.notes.get(state.notes.selected);

export const draftNote = (status: IState) => status.notes.draft;
