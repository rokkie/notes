import {applyMiddleware, createStore} from 'redux';
import {createEpicMiddleware} from 'redux-observable';
import {composeWithDevTools} from 'redux-devtools-extension';
import epics from './epics';
import reducers from './reducers';

const epicMiddleware = createEpicMiddleware();

export default createStore(reducers, composeWithDevTools(applyMiddleware(epicMiddleware)));

epicMiddleware.run(epics);
