import { action, createAsyncAction } from 'typesafe-actions';
import * as types from './types';
import { INote, INoteDraft } from '../../models';

export const fetchNotes = createAsyncAction(
  types.NOTES_FETCH_REQUEST,
  types.NOTES_FETCH_SUCCESS,
  types.NOTES_FETCH_FAILURE,
  types.NOTES_FETCH_CANCEL,
)<void, INote[], Error, string>();
export const selectNote = (id: number) => action(types.NOTES_SELECT, id);
export const editNote = (id: number) => action(types.NOTES_EDIT, id);
export const createDraft = (draft: INoteDraft) => action(types.NOTES_CREATE_DRAFT, draft);

export const saveNote = createAsyncAction(
  types.NOTES_SAVE_REQUEST,
  types.NOTES_SAVE_SUCCESS,
  types.NOTES_SAVE_FAILURE,
  types.NOTES_SAVE_CANCEL,
)<INoteDraft, INote, Error, string>();