export const NOTES_FETCH_REQUEST = '@notes/ NOTES_FETCH_REQUEST';
export const NOTES_FETCH_SUCCESS = '@notes/NOTES_FETCH_SUCCESS';
export const NOTES_FETCH_FAILURE = '@notes/NOTES_FETCH_FAILURE';
export const NOTES_FETCH_CANCEL = '@notes/NOTES_FETCH_CANCEL';

export const NOTES_SELECT = '@notes/NOTES_SELECTED';
export const NOTES_EDIT = '@notes/NOTES_EDIT';
export const NOTES_CREATE_DRAFT = '@notes/NOTES_CREATE_DRAFT';


export const NOTES_SAVE_REQUEST = '@notes/NOTES_SAVE_REQUEST';
export const NOTES_SAVE_SUCCESS = '@notes/NOTES_SAVE_SUCCESS';
export const NOTES_SAVE_FAILURE = '@notes/NOTES_SAVE_FAILURE';
export const NOTES_SAVE_CANCEL = '@notes/NOTES_SAVE_CANCEL';

