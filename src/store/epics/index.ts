import { combineEpics } from 'redux-observable';
import * as notes from './notes';

export default combineEpics(
  ...Object.values(notes)
);
