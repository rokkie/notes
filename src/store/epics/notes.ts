import { Action } from 'redux';
import { of } from 'rxjs';
import { ActionsObservable } from 'redux-observable';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import { fetchNotes as fetchNotesAction, saveNote as saveNoteAction } from '../actions';
import { INote } from '../../models';
import { fromFetch } from 'rxjs/fetch';

type ErrorResponse = { message: string; };

export const fetchNotes = (action$: ActionsObservable<Action<any>>) => action$.pipe(
  filter(isActionOf(fetchNotesAction.request)),
  switchMap((action) => fromFetch('/notes?order=date_created.desc', {
    headers: { Accept: 'application/json' }
  }).pipe(switchMap(res => res.json()))),
  map(fetchNotesAction.success)
);

export const saveNote = (action$: ActionsObservable<Action<any>>) => action$.pipe(
  filter(isActionOf(saveNoteAction.request)),
  switchMap(({payload}) => fromFetch(`/notes`, {
    method: payload.id ? 'PUT' : 'POST',
    headers: {
      Accept        : 'application/vnd.pgrst.object+json',
      'Content-Type': 'application/json',
      Prefer        : 'return=representation',
    },
    body: JSON.stringify(Object.assign({
        title: payload.title,
        body : payload.body,
      }, payload.id ? {id: payload.id} : {},
      payload.dateModified ? {date_modified: payload.dateModified} : {})),
  }).pipe(
    switchMap(async (res): Promise<INote> => {
      const data: INote | ErrorResponse = await res.json();

      if (res.ok) { return data as INote; }
      throw new Error((data as ErrorResponse).message);
    })
  )),
  map(saveNoteAction.success),
  catchError(err => of(saveNoteAction.failure(err)))
);
