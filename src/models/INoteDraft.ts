
export interface INoteDraft {
  readonly id?: number;
  title?: string,
  body?: string,
  dateModified?: Date,
}
