
export interface INote {
  id: number,
  title: string,
  body: string,
  dateCreated: Date,
  dateModified?: Date,
}
