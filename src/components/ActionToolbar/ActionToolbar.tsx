import * as React from 'react';
import { useSelector } from 'react-redux';
import styles from './ActionToolbar.module.scss';
import { selectedNote } from '../../store/selectors';
import { INoteDraft } from '../../models';

interface IActionToolbarProps {
  onAddBtnClick: (draft: INoteDraft) => void;
  onEditBtnClick: (id: number) => void;
}

const ActionToolbar: React.FC<IActionToolbarProps> = (props) => {
  const selected = useSelector(selectedNote);

  return (
    <div role="toolbar"
         className={styles.actionToolbar}>
      <button disabled={!selected} onClick={() => selected && props.onEditBtnClick(selected.id)}>Edit Note</button>
      <button onClick={() => props.onAddBtnClick({
        title: '',
        body : '',
      })}>Add Note</button>
    </div>
  );
};

export default ActionToolbar;