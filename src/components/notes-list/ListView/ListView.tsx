import * as React from 'react';
import styles from './ListView.module.scss';

interface IListViewProps {
}

const ListView: React.FC<IListViewProps> = (props) => (
  <ul className={styles.listView}>
    {props.children}
  </ul>
);

export default ListView;
