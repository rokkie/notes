import * as React from 'react';
import { INote } from '../../../models';
import styles from './ListItem.module.scss';

interface IListItemProps {
  item: INote,
  onListItemClick: (id: number) => void;
}

const ListItem: React.FC<IListItemProps> = (props) => (
  <li className={styles.listItem}
      onClick={() => props.onListItemClick(props.item.id)}>
    {props.item.title}
  </li>
);

export default ListItem;
