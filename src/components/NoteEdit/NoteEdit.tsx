import * as React from 'react';
import { useDispatch } from 'react-redux';
import styles from './NoteEdit.module.scss';
import { INoteDraft } from '../../models';
import { createDraft, saveNote } from '../../store/actions';

interface INoteEditProps {
  draft: INoteDraft;
}

const NoteEdit: React.FC<INoteEditProps> = (props) => {
  const dispatch      = useDispatch(),
        onTitleChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
          const draft = Object.assign({}, props.draft, {title: evt.target.value});

          dispatch(createDraft(draft));
        },
        onBodyChange  = (evt: React.ChangeEvent<HTMLTextAreaElement>) => {
          const draft = Object.assign({}, props.draft, {body: evt.target.value});

          dispatch(createDraft(draft));
        },
        onBtnClick    = (evt: React.MouseEvent<HTMLButtonElement>) => {
          evt.preventDefault();

          const dateModified = props.draft.id ? {dateModified: new Date()} : {},
                draft        = Object.assign({}, props.draft, dateModified);

          dispatch(saveNote.request(draft));
        };

  return (
    <form className={styles.noteEdit}>
      <input value={props.draft && props.draft.title}
             onChange={onTitleChange}/>
      <textarea value={props.draft && props.draft.body}
                onChange={onBodyChange}/>
      <button onClick={onBtnClick}>
        Save
      </button>
    </form>
  );
};

export default NoteEdit;
