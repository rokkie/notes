import * as React from 'react';
import styles from './NoteDetail.module.scss';
import { INote } from '../../models';

interface INoteDetailProps {
  note: INote;
}

const NoteDetail: React.FC<INoteDetailProps> = (props) => {
  return (
    <div className={styles.noteDetail}>
      <h2>{props.note.title}</h2>
      <div>{props.note.body}</div>
    </div>
  );
};

export default NoteDetail;
